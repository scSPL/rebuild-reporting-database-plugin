﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Navigation;
using System.Xml;
using Ionic.Zip;
using Sitecore.Diagnostics;
using SIM;
using SIM.Adapters.SqlServer;
using SIM.Adapters.WebServer;
using SIM.FileSystem;
using SIM.Instances;
using SIM.Pipelines;
using SIM.Pipelines.Install;
using SIM.Tool.Base;
using SIM.Tool.Base.Profiles;

namespace RebuildReportingDBPlugin
{
    public class RebuildDatabaseButton : SIM.Tool.Base.Plugins.IMainWindowButton
    {
        private string ReportingSecondaryFolder { get; set; }
        private string DatabaseName { get; set; }
        private SqlConnectionStringBuilder DefaultConnectionString = ProfileManager.GetConnectionString();

        public bool IsEnabled(Window mainWindow, Instance instance)
        {
            return true;
        }

        public void OnClick(Window mainWindow, Instance instance)
        {
            if (!CheckVersion(instance))
            {
                return;
            }
            if (instance.Configuration.ConnectionStrings.Any(x => x.Name == "reporting.secondary"))
            {
                //if the reporting.secondary connection string exist we just open the rebuild reporting database page.
                InstanceHelperEx.BrowseInstance(instance, mainWindow, "/sitecore/admin/rebuildreportingdb.aspx", true,
                    null, new string[0]);
            }
            else
            {
                DatabaseName =  SqlServerManager.Instance.GenerateDatabaseRealName(instance.Name, "reporting_secondary", null, null);
                ReportingSecondaryFolder = Path.Combine(instance.RootPath, "Databases\\Reporting Secondary");
                ResolveConflicts(mainWindow);
                ExtractFiles(instance, mainWindow);

                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(DefaultConnectionString.ToString())
                {
                    InitialCatalog = DatabaseName
                };

                instance.Configuration.ConnectionStrings.Add("reporting.secondary", builder);
                SqlServerManager.Instance.AttachDatabase(DatabaseName,Path.Combine(ReportingSecondaryFolder, "Sitecore.Analytics.mdf"), builder, true);
                InstanceHelperEx.BrowseInstance(instance, mainWindow, "/sitecore/admin/rebuildreportingdb.aspx", true,
                    null, new string[0]);
            }
        }

        public virtual bool CheckVersion(Instance instance)
        {
            if (instance == null)
            {
                return false;
            }
            var version = instance.Product.Version.SubstringEx(0, 3);
            if (version[0] < '7' || (version[0] == '7' && version[2] != '5'))
            {
                WindowHelper.ShowMessage("Instance version is lower then 7.5. Rebuild process is terminated.");
                return false;
            }
            return true;
        }

        private void ExtractFiles(Instance instance, Window mainWindow)
        {
            string pathToDatabaseFile = Path.Combine(ReportingSecondaryFolder, "Sitecore.Analytics.mdf");
            if (FileSystem.Local.File.Exists(pathToDatabaseFile))
            {
                string[] options = new string[] { "Use this file", "Replace a file by a clear one" };
                string result = WindowHelper.AskForSelection("Select option", null,"The {0} file already exists, do you want to use this file or replace it by a clear one?".FormatWith(new object[] { Path.Combine(ReportingSecondaryFolder, "Sitecore.Analytics.mdf") }), options, mainWindow, null, null, null);
                switch (result)
                {
                    case "Use this file":
                        return;
                    default:
                        break;
                }
                var databaseName = SqlServerManager.Instance.GetDatabaseNameFromFile(DefaultConnectionString, pathToDatabaseFile);
                if (!string.IsNullOrEmpty(databaseName))
                {
                    SqlServerManager.Instance.DeleteDatabase(databaseName,DefaultConnectionString);
                }
                FileSystem.Local.File.Delete(pathToDatabaseFile);
            }
            string tempFolderName = Path.Combine(ReportingSecondaryFolder, "Temp");
            string mdfFilePath = ZipUnpackFile(instance.Product.PackagePath, tempFolderName, "Sitecore.Analytics.mdf");
            string ldfFilePath = ZipUnpackFile(instance.Product.PackagePath, tempFolderName, "Sitecore.Analytics.ldf");
            FileSystem.Local.File.Move(mdfFilePath, Path.Combine(ReportingSecondaryFolder, "Sitecore.Analytics.mdf"));
            FileSystem.Local.File.Move(ldfFilePath, Path.Combine(ReportingSecondaryFolder, "Sitecore.Analytics.ldf"));
            FileSystem.Local.Directory.DeleteIfExists(tempFolderName, null);
        }

        private void ResolveConflicts(Window mainWindow)
        {
            if (SqlServerManager.Instance.DatabaseExists(DatabaseName, DefaultConnectionString)) 
            {
                //Database with such name exists in SQL server
                string databaseFileName = SqlServerManager.Instance.GetDatabaseFileName(DatabaseName, DefaultConnectionString);
                if (string.IsNullOrEmpty(databaseFileName))
                {
                    //File does not exists.
                    string message = "The database with the same '{0}' name is already exists in the SQL Server metabase but points to non-existing file. ".FormatWith(new object[] { DatabaseName });
                    string[] options = new string[] { "Delete", "Terminate" };
                    string result = WindowHelper.AskForSelection("Select option", null, message + "Would you like to delete it?", options, mainWindow, null, null, null);
                    switch (result)
                    {
                        case "Delete":
                            SqlServerManager.Instance.DeleteDatabase(DatabaseName, DefaultConnectionString);
                            return;
                        default:
                            throw new Exception(message);
                    }
                }
                //File exists
                string[] options2 = new string[] { "Delete the '{0}' database".FormatWith(new object[] { DatabaseName }), "Use another database name", "Terminate current action" };
                string message2 = "The database with '{0}' name already exists".FormatWith(new object[] { DatabaseName });
                string result2 = WindowHelper.AskForSelection("Select option", null, message2, options2, mainWindow, null, null, null);
                if (result2 != null)
                {
                    if (result2 == "Terminate current action")
                    {
                        throw new Exception(message2);
                    }
                    if (result2 == "Use another database name")
                    {
                        DatabaseName = GetUnusedDatabaseName();
                        return;
                    }
                }
                //Delete from SQL server
                //SqlServerManager.Instance.DeleteDatabase(DatabaseName, DefaultConnectionString);
            }
        }

        public string ZipUnpackFile(string pathToZip, string pathToUnpack, string fileName)
        {
            string str = pathToZip;
            string baseDirectory = pathToUnpack;
            using (ZipFile file = ZipFile.Read(str))
            {
                foreach (ZipEntry entry in file)
                {
                    string[] strArray = entry.FileName.Split(new char[] { '/' });
                    if (strArray[strArray.Length - 1] == fileName)
                    {
                        entry.Extract(baseDirectory, ExtractExistingFileAction.OverwriteSilently);
                        return Path.Combine(pathToUnpack, entry.FileName);
                    }
                }
            }
            return "";
        }

        private string GetUnusedDatabaseName()
        {
            Assert.ArgumentNotNull(DefaultConnectionString, "defaultConnectionString");
            Assert.ArgumentNotNull(DatabaseName, "databaseName");
            for (int i = 1; i <= 100; i++)
            {
                if (!SqlServerManager.Instance.DatabaseExists(DatabaseName + '_' + i, DefaultConnectionString))
                {
                    return (DatabaseName + "_" + i);
                }
            }
            throw new InvalidOperationException("Something weird happen... Do you really have '{0}' file?".FormatWith(new object[] { DatabaseName + "_" + 100 }));
        }



    }
}
