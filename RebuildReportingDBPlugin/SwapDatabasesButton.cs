﻿using System.Data.SqlClient;
using System.Linq;
using SIM.Tool.Base;
using SIM.Tool.Base.Profiles;

namespace RebuildReportingDBPlugin
{
    class SwapDatabasesButton : SIM.Tool.Base.Plugins.IMainWindowButton
    {
        public bool IsEnabled(System.Windows.Window mainWindow, SIM.Instances.Instance instance)
        {
            return true;
        }

        public void OnClick(System.Windows.Window mainWindow, SIM.Instances.Instance instance)
        {
            var defaultConnectionString = ProfileManager.GetConnectionString();
            if (instance.Configuration.ConnectionStrings.All(x => x.Name != "reporting"))
            {
                WindowHelper.ShowMessage("reporting database connection string does not exist. Operation terminated.");
                return;
            }
            if (instance.Configuration.ConnectionStrings.All(x => x.Name != "reporting.secondary"))
            {
                WindowHelper.ShowMessage("reporting secondary database connection string does not exist. Operation terminated.");
                return;
            }
            SqlConnectionStringBuilder builder1 = new SqlConnectionStringBuilder(defaultConnectionString.ToString())
            {
                InitialCatalog = instance.Configuration.ConnectionStrings.FirstOrDefault(x=> x.Name == "reporting").RealName
            };
            SqlConnectionStringBuilder builder2 = new SqlConnectionStringBuilder(defaultConnectionString.ToString())
            {
                InitialCatalog = instance.Configuration.ConnectionStrings.FirstOrDefault(x => x.Name == "reporting.secondary").RealName
            };
            instance.Configuration.ConnectionStrings.Add("reporting.secondary", builder1);
            instance.Configuration.ConnectionStrings.Add("reporting", builder2);

        }

    }
}
